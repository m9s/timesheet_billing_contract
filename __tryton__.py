# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
{
    'name': 'Timesheet Billing Contract',
    'name_de_DE': 'Zeiterfassung Abrechnung Vertragsverwaltung',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz',
    'description': '''
    - Integrates billing on timesheets with contracts
    ''',
    'description_de_DE': '''
    - Intergriert die Abrechnung auf Zeiterfassungblättern mit Verträgen
    ''',
    'depends': [
        'contract',
        'timesheet_billing',
    ],
    'xml': [
    ],
    'translation': [
    ],
}
